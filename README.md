# Tarefas Computação Gráfica #

Tarefas 1 e 2 do Tópico 2 e 1 do Tópico 3

### HelloGLFW-3D - Camera - x86 ###

* Crie uma cena com um "chão" (plano) e um cubo 3D (sugestão: pinte cada face do cubo com uma cor diferente)
* Use as teclas 1 a 5 para mudar a posição e orientação da câmera, de maneira que possamos ver o cubo de frente, de trás, da esquerda, da direita e de topo.

### OpenGL-3D-Camera-Scene - x64 ###

* Implementação da movimentação da câmera usando teclado e mouse
 -- Use as teclas  (setas e/ou WASD) para movimentar a câmera para frente, para trás e para os lados
 - Use o mouse para mudar a direção
 - Use a rodinha do mouse (ou escolha teclas) para dar Zoom In e Zoom Out.